from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('hackerman/', include('hackerman.urls')),
    path('admin/', admin.site.urls),
]